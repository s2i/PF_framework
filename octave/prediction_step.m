function particles = prediction_step(particles, u, noise)
% Updates the particles by drawing from the motion model
% Use u.r1, u.t, and u.r2 to access the rotation and translation values
% which have to be pertubated with Gaussian noise.
% The position of the i-th particle is given by the 3D vector
% particles(i).pose which represents (x, y, theta).

% noise parameters
% Assume Gaussian noise in each of the three parameters of the motion model.
% These three parameters may be used as standard deviations for sampling.
r1Noise = noise(1);
transNoise = noise(2);
r2Noise = noise(3);

numParticles = length(particles);


for i = 1:numParticles

  % append the old position to the history of the particle
  particles(i).history{end+1} = particles(i).pose;
particles(i).pose(1)=particles(i).history{1}(1)+randn(1);
particles(i).pose(2)=particles(i).history{1}(2)+randn(1);
particles(i).pose(3)=particles(i).history{1}(3)+randn(1);

  % TODO: sample a new pose for the particle
   %particles(i).history{1} posicao no tempo 1  (1)-x (2)-y (3)-theta 
  
 %  w=target/proposal
   
   %particles(i).pose= particles(i).history(1)+particles(i).pose,
  
  %previousX=mu(1);
%previousY=mu(2);
previousTheta=particles(i).history{1}(3);
dt=u.r1+u.r2;

va1=-u.t * sin(previousTheta)+ u.t*sin(previousTheta+dt); %X
va2=u.t * cos(previousTheta) - u.t*cos(previousTheta+dt); %Y
va3=normalize_angle(previousTheta+dt); %theta normalizado
  
particles(i).pose(1)=particles(i).pose(1)+va1+r1Noise;
particles(i).pose(2)=particles(i).pose(2)+va1+r2Noise;
particles(i).pose(3)=particles(i).pose(3)+va1+transNoise;
particles(i).pose(3)=normalize_angle(particles(i).pose(3));

end

end
